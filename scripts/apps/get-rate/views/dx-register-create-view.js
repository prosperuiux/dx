var DXRegisterCreateView = DXRegisterOfferView.extend({

    onRender: function () {
        this.ui.heading.text('Register Create Page')
    },

    loadUser: function (data) {
        var formModel = new OfferModel(data);
        var formView = new RegisterCreateFormView({
            model: formModel
        });
        if (formModel.isValid()) {
            this.once('render', function () {
                this.formRegion.show(formView);
            });
        }
    }

});