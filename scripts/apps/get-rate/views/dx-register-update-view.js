var DXRegisterUpdateView = DXRegisterOfferView.extend({


    onRender: function () {
        this.ui.heading.text('Register Update Page')
    },

    loadUser: function (data) {
        var formModel = new OfferModel(data);
        var formView = new RegisterUpdateFormView({
            model: formModel
        });
        if (formModel.isValid()) {
            this.once('render', function () {
                this.formRegion.show(formView);
            });
        }
    },

    showExpiredNotice: function () {

    }

});
