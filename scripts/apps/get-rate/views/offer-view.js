var OfferView = Backbone.Marionette.ItemView.extend({

    template: _.template('<h2>Offer</h2><table class="table table-bordered"><tr><td>Amount</td><td><%= amount %></td></tr><tr><td>APR</td><td><%= apr %></td></tr></table>')

});