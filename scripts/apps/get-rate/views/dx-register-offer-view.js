var DXRegisterOfferView = Backbone.Marionette.LayoutView.extend({

    template: _.template('<div class="page-header"><h1></h1></div><div id="offer"></div><div id="form"></div>'),

    ui: {
        heading: '.page-header h1'
    },

    initialize: function () {
        this.once('render', function () {
            this.addRegion('offerRegion', '#offer');
            this.addRegion('formRegion', '#form');
        });
    },

    loadOffer: function (data) {
        var offerModel = new OfferModel(data);
        var offerView = new OfferView({
            model: offerModel
        });
        if (offerModel.isValid()) {
            this.once('render', function () {
                this.offerRegion.show(offerView);
            });
        }
    },

    hasOffer: function() {
        return this.offerRegion.hasView();
    }

});