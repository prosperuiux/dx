var DXRouter = Backbone.Marionette.AppRouter.extend({

    appRoutes: {
        'dx/register': 'register',
        'dx/register/offer': 'registerOffer',
        'dx/register/expired': 'registerExpired',
        'dx/create-password': 'createPassword',
        'dx/create-password/offer': 'createPasswordOffer',
        'dx/create-password/expired': 'createPasswordExpired'
    },

    routeOnProspect: function (prospect) {

        var page = 'create-password';
        var state = '';

        if (prospect.isExistingUser()) {
            page = 'register';
        }

        if (prospect.hasOffer()) {
            state = '/offer';
        } else if (prospect.offerExpired()) {
            state = '/expired';
        }

        this.navigate('dx/' + page + state, {trigger:true} );
    }

});