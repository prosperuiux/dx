var OfferModel = Backbone.Model.extend({
    defaults: {
        amount: 0.0,
        apr: 0.0
    }
});