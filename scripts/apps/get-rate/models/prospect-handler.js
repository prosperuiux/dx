var ProspectHandler = Backbone.Model.extend({

    url: 'mocks/prospect.json.txt',

    defaults: {
        first_name: '',
        email: '',
        offer_expired: false,
        has_offer: false,
        existing_user: false,
        offer_amount: 0,
        offer_apr: 0
    },

    parse: function (data) {
        return {
            first_name: data.first_name,
            email: data.email,
            has_offer: data.has_offer,
            existing_user: data.existing_user,
            offer_expired: data.offer_expired,
            offer_amount: data.offer.amount,
            offer_apr: data.offer.apr
        };
    },

    isExistingUser: function () {
        return this.get('existing_user');
    },

    offerExpired: function () {
        return this.get('offer_expired');
    },

    hasOffer: function () {
        return this.get('has_offer');
    },

    getUser: function () {
        return {
            first_name: this.get('first_name'),
            email: this.get('email')
        };
    },

    getOffer: function () {
        return {
            amount: this.get('offer_amount'),
            apr: this.get('offer_apr')
        };
    }

});
