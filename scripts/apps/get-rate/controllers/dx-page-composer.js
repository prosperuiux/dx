var DXPageComposer = Backbone.Marionette.Controller.extend({

    views: {
        dx_register: new DXRegisterUpdateView(),
        dx_create_pass: new DXRegisterCreateView()
    },

    models: {
        offer: new Backbone.Model(),
        register: new Backbone.Model()
    },

    initialize: function (options) {
        this.prospect = options.prospect;
    },

    pushView: function (view) {
        this.trigger('page:ready', view);
    },

    register: function () {
        this.views.dx_register.loadUser(this.models.register.attributes);
        this.pushView(this.views.dx_register);
    },

    registerOffer: function () {
        this.views.dx_register.loadUser(this.models.register.attributes);
        this.views.dx_register.loadOffer(this.models.offer.attributes);
        this.pushView(this.views.dx_register);
    },

    registerExpired: function () {
        this.views.dx_register.showExpiredNotice();
        this.pushView(this.views.dx_register);
    },

    createPassword: function () {
        this.views.dx_create_pass.loadUser(this.models.register.attributes)
        this.pushView(this.views.dx_create_pass);
    },

    createPasswordOffer: function () {
        this.views.dx_create_pass.loadUser(this.models.register.attributes)
        this.views.dx_create_pass.loadOffer(this.models.offer.attributes);
        this.pushView(this.views.dx_create_pass);

    },

    createPasswordExpired: function () {}

});