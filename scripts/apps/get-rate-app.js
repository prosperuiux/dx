var GetRateApp = Backbone.Marionette.Application.extend({

    regions: {
        pageRegion: '#page'
    },

    initialize: function () {
        this.prospect = new ProspectHandler();
        this.pageComposer = new DXPageComposer();
        this.routers = {
            dx: new DXRouter({
                controller: this.pageComposer
            })
        };
    },

    onBeforeStart: function () {
        this.prospect.on('sync', this.loadProspectData, this);
        this.prospect.on('sync', this.routeUser, this);
        this.pageComposer.on('page:ready', function (view) {
            this.pageRegion.show(view);
        }, this);
    },

    loadProspectData: function (prospect) {
        this.pageComposer.models.offer.set(prospect.getOffer());
        this.pageComposer.models.register.set(prospect.getUser());
    },

    routeUser: function (prospect) {
        if (Backbone.history) {
            Backbone.history.start();
        }
        this.routers.dx.routeOnProspect(prospect);
    },

    onStart: function () {
        this.prospect.fetch();
    }

});