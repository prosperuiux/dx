module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        connect: {
            server: {
                options: {
                    base: '.',
                    hostname: 'localhost',
                    port: 9001,
                    livereload: true
                }
            }
        },
        watch: {
            scripts: {
                files: ['scripts/**/*.js', 'mocks/*.txt'],
                tasks: ['jshint'],
                options: {
                    livereload: true
                }
            }
        },
        jshint: {
            all: ['Gruntfile.js', 'scripts/**/*.js']
        }
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['jshint']);
    grunt.registerTask('serve', ['connect:server', 'watch']);

};