{
    "first_name": "Franklin",
    "email": "fclark@prosper.com",
    "existing_user": true,
    "offer_expired": false,
    "has_offer": false,
    "offer": {
        "amount": 20.0,
        "apr": 6.59
    }
}